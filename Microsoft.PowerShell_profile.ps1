# https://gist.github.com/xPaw/15fe59e92a00ab1d4004fb05b37764ff

$env:DOTNET_CLI_TELEMETRY_OPTOUT = "1"
$env:POWERSHELL_TELEMETRY_OPTOUT = "1"
$env:POWERSHELL_UPDATECHECK = "Off"

Set-Alias -Name l -Value Get-ChildItem

function .. { Set-Location .. }
function ... { Set-Location ../.. }
function .... { Set-Location ../../.. }
function ..... { Set-Location ../../../.. }
function ...... { Set-Location ../../../../.. }

Invoke-Expression -Command $(gh completion -s powershell | Out-String)

Register-ArgumentCompleter -Native -CommandName dotnet -ScriptBlock {
	param($wordToComplete, $commandAst, $cursorPosition)

	dotnet complete --position $cursorPosition "$commandAst" | ForEach-Object {
		[System.Management.Automation.CompletionResult]::new($_, $_, 'ParameterValue', $_)
	}
}

function script:Get-PromptGitStatus {
	$originalExitCode = $LASTEXITCODE

	$gitStatus = git --no-optional-locks status --branch --porcelain=v2 -z --ignore-submodules=dirty 2>$null
	$gitExitCode = $LASTEXITCODE

	$global:LASTEXITCODE = $originalExitCode

	if ($gitExitCode -ne 0) {
		return
	}

	$statusLines = $gitStatus -split "`0"

	$branchName = $null
	$isDirty = $false

	foreach ($line in $statusLines) {
		if ($line) {
			if ($line.StartsWith('# branch.head')) {
				$branchName = $line.Substring(14)
			}
			elseif (-not $line.StartsWith('#')) {
				$isDirty = $true
				break
			}
		}
	}

	if ($null -eq $branchName) {
		return
	}

	$status = if ($isDirty) { "$([char]0x1b)[31mx" } else { "$([char]0x1b)[32mo" }

	return "$([char]0x1b)[0mon $([char]0x1b)[34mgit$([char]0x1b)[36m:$branchName $status$([char]0x1b)[0m"
}

function script:Get-PromptShortPath {
	$currentPath = (Get-Location).Path
	$homePath = $HOME

	if ($currentPath.StartsWith($homePath)) {
		$currentPath = "~" + $currentPath.Substring($homePath.Length)
	}

	$currentPath.Replace('\', '/')
}

function Prompt {
	$isAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")

	$prompt = @(
		if ($isAdmin) {
			"`n$([char]0x1b)[1;31m%$([char]0x1b)[0m $([char]0x1b)[30;43m$env:USERNAME$([char]0x1b)[0m"
		}
		else {
			"`n$([char]0x1b)[1;34m#$([char]0x1b)[0m $([char]0x1b)[36m$env:USERNAME$([char]0x1b)[0m"
		}

		"$([char]0x1b)[0m@"
		"$([char]0x1b)[32m$env:COMPUTERNAME$([char]0x1b)[0m"

		"in"
		"$([char]0x1b)[1;33m$(Get-PromptShortPath)$([char]0x1b)[0m"

		(Get-PromptGitStatus)

		"[$([datetime]::Now.ToString('HH:mm:ss'))]"

		# Add exit code only if it's non-zero
		if (($null -ne $LASTEXITCODE) -and ($LASTEXITCODE -ne -1) -and ($LASTEXITCODE -ne 0)) {
			"C:$([char]0x1b)[31m$LASTEXITCODE$([char]0x1b)[0m"
		}

		"`n$([char]0x1b)[1;31m`$$([char]0x1b)[0m "
	)

	$prompt -join ' '
}
